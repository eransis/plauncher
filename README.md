<p align="center">
    <img src="http://codeberg.org/glowiak/plauncher/raw/branch/master/branding/logo.png">
</p>

# plauncher

PLauncher (pronounced: /pɛlˈant͡ʃɛr/) is a launcher for the popular game Minecraft, specifically for old versions (<1.3) and mods for it (mostly using ModLoader).

### Features

    -BetaCraft proxy by default
    -no patches needed to join old servers like AlphaPlace
    -written from scratch in Python 3 with the use of the Qt5 graphics library
    -does not run on windows

### Why?

Well, this launcher tries to be simple, customizable and separated.

It uses Shell for internals and Python/PyQt5 for Gui.

### Roadmap

[DONE] make it working

[DONE] make a qt ui

[CANCELLED] make a wx ui

[DONE] saving settings in guis

[DONE] version index & jar downloading

[WIP] eventually rewrite all internals to python

[DONE] in-launcher Forge installation

[WIP] M$ accounts

[REMOVED] splashes

[DONE] translations

[DONE] window logo

[WIP] eventually implement downloading mods from mcarchive.net

[DONE] adding mods to jar

[DONE] mod menu like PolyMC has

[DONE] live-patching jars like MagicLauncher does

Supported versions: up to 1.6.4

### Unwanted features

Windows support. Because nobody uses that crap B)

Electron. A launcher does not need to be an embedded web browser.

### Modding

Starting from 1.1.6, we do have a mods menu available by clicking the 'Mods' button. Here you can add or remove that old mods that were added to minecraft.jar. No winrar needed, just click 'Add', select file and reopen the mods window. Click run to start game.

You can also install Forge for 1.1+ by clicking the 'Install Forge' button. Then the forged version is available as version_number-forge

### Downloading and running

Clone the git repo, and run ./QtLauncher.py. You probably need to adjust settings here, when you are done, click 'Save Settings'. Now click 'Play' and enjoy!

### Linux with GNU coreutils

If your Linux distro uses GNU tar, then starting game will probably fail. To fix this, change 'tar' to 'bsdtar' (and install it of course) in the Advanced tab.

### M$ Login

ok, with trying to get 1.7 to work here, i will need to figure out microsoft login. I want to try to join WOTRMC with plauncher, but it's a premium server, so i'm kinda forced to add it

### Rebranding

Starting with 1.0.1, the launcher can be rebranded by editing the branding/Application.py file. Feel free to fork

The branding/logo.png must be a 366x194 PNG image with the launcher logo that displays on the bottom of the launcher.

[Here](https://codeberg.org/glowiak/sUPERprohaXXXXX_LAUNCHER) is an example noobish rebranding.

### Translations

Starting with 1.1.0, the launcher supports translations that you can change in the Advanced settings. At now there are only english and Polish, pull requests are welcome!

### Wx GUI support

Maintaining more than one GUI is hard, some things that do work with Qt do not work with Wx (like the wx.FileDialog()). So I decided to stop maintaining the Wx gui. I will keep it however in the repo, but it may not work due to changes in other scripts.

### Credits

logo uses the [minecraft evenings font](https://www.fontspace.com/minecraft-evenings-font-f17735) and it's Made with GIMP

launcher uses PyQT/wxPython and [Python 3](http://python.org)

the launcher was written with [geany](http://geany.org) and [vim](http://vim.org)

the core scripts are written in [bash](http://gnu.org/software/bash)

### bla bla bla

I am not associated with Mojang nor Microsoft, Minecraft is a property of Mojang AB.
