#!/bin/sh
# CLI for PLauncher
# A Launcher that sucks less
HERE=$(readlink -f $(dirname $0))

# user choices
JAVA=$(cat $HERE/../var/java)
NICK=$(cat $HERE/../var/nick)
MEM=$(cat $HERE/../var/mem)
OS=$(cat $HERE/../var/os)
VJAR=${VJAR:-/tmp/version.jar} # PRECLASSIC/CLASSIC/INDEV/INFDEV/ALPHA/BETA/RELEASE
VERSION=${VERSION:-1.0.2}

getVersionJar()
{
    C=$2
    V=$3
    H=$1
    echo $H/../versions/$C/$V.jar
}

CHANNEL=$(echo $CHANNEL | tr [a-z] [A-Z])

# check if the jars are installed

if [ ! -f "$HERE/../jars/lwjgl-2.9.0.jar" ]
then
    echo "Downloading jars..."
    cd $HERE/../jars
    wget --no-check-certificate --input-file=filelist.txt
fi

# check if natives are installed

if [ ! -f "$HERE/../natives/$OS/liblwjgl.so" ]
then
    echo "Unpacking natives..."
    for b in $(ls $HERE/../jars/*platform*${OS}*.jar)
    do
        $(cat $HERE/../var/tar) xvf $b -C $HERE/../natives/$OS/
    done
    rm -rvf $HERE/../natives/$OS/META-INF
fi
# launch the game

JAVAP=$JAVA sh $HERE/runjar.sh /tmp/*modded_2022.jar $NICK $MEM $OS $VERSION
