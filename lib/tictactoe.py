#!/usr/bin/env python3
import os, sys, random
from PyQt5.QtCore import *
from PyQt5.QtGui import *
from PyQt5.QtWidgets import *

class App(QMainWindow):
    def __init__(self):
        super().__init__()
        self.w = QWidget()
        self.w.setWindowTitle("pyTicTacToe")
        self.w.setGeometry(100,100,300,330)
        
        self.turn = "X"
        self.gameBlocked = False
        self.Butt = [ "", "", "", "", "", "", "", "", "", "" ]
        
        # top line
        self.Butt[1] = QPushButton(self.w)
        self.Butt[1].move(0,0)
        self.Butt[1].resize(100,100)
        self.Butt[2] = QPushButton(self.w)
        self.Butt[2].move(100,0)
        self.Butt[2].resize(100,100)
        self.Butt[3] = QPushButton(self.w)
        self.Butt[3].move(200,0)
        self.Butt[3].resize(100,100)
        
        # middle line
        self.Butt[4] = QPushButton(self.w)
        self.Butt[4].move(0,100)
        self.Butt[4].resize(100,100)
        self.Butt[5] = QPushButton(self.w)
        self.Butt[5].move(100,100)
        self.Butt[5].resize(100,100)
        self.Butt[6] = QPushButton(self.w)
        self.Butt[6].move(200,100)
        self.Butt[6].resize(100,100)
        
        # bottom line
        self.Butt[7] = QPushButton(self.w)
        self.Butt[7].move(0,200)
        self.Butt[7].resize(100,100)
        self.Butt[8] = QPushButton(self.w)
        self.Butt[8].move(100,200)
        self.Butt[8].resize(100,100)
        self.Butt[9] = QPushButton(self.w)
        self.Butt[9].move(200,200)
        self.Butt[9].resize(100,100)
        
        # turn signalization
        self.turnText = QLabel(self.w)
        self.turnText.move(5,310)
        self.turnText.setText(f"Turn: {self.turn}")
        
        # who win
        self.winText = QLabel(self.w)
        self.winText.move(190, 310)
        self.winText.setText("Nobody has won!") # X/O has won!
        
        # set button actions
        self.Butt[1].clicked.connect(self.Butt1Click)
        self.Butt[2].clicked.connect(self.Butt2Click)
        self.Butt[3].clicked.connect(self.Butt3Click)
        self.Butt[4].clicked.connect(self.Butt4Click)
        self.Butt[5].clicked.connect(self.Butt5Click)
        self.Butt[6].clicked.connect(self.Butt6Click)
        self.Butt[7].clicked.connect(self.Butt7Click)
        self.Butt[8].clicked.connect(self.Butt8Click)
        self.Butt[9].clicked.connect(self.Butt9Click)
        
        # the reset button
        self.reB = QPushButton(self.w)
        self.reB.setText("Reset")
        self.reB.resize(100,20)
        self.reB.move(85,310)
        self.reB.clicked.connect(self.resetGame)
        
        self.w.show()

    def Butt1Click(self):
        if self.turn == "X":
            if self.Butt[1].text() != "X" and self.Butt[1].text() != "O" and self.gameBlocked != True:
                self.Butt[1].setText("X")
                self.setTurn("O")
                self.enemyBot()
    
    def Butt2Click(self):
        if self.turn == "X":
            if self.Butt[2].text() != "X" and self.Butt[2].text() != "O" and self.gameBlocked != True:
                self.Butt[2].setText("X")
                self.setTurn("O")
                self.enemyBot()
        
    def Butt3Click(self):
        if self.turn == "X":
            if self.Butt[3].text() != "X" and self.Butt[3].text() != "O" and self.gameBlocked != True:
                self.Butt[3].setText("X")
                self.setTurn("O")
                self.enemyBot()
    
    def Butt4Click(self):
        if self.turn == "X":
            if self.Butt[4].text() != "X" and self.Butt[4].text() != "O" and self.gameBlocked != True:
                self.Butt[4].setText("X")
                self.setTurn("O")
                self.enemyBot()
    
    def Butt5Click(self):
        if self.turn == "X":
            if self.Butt[5].text() != "X" and self.Butt[5].text() != "O" and self.gameBlocked != True:
                self.Butt[5].setText("X")
                self.setTurn("O")
                self.enemyBot()
    
    def Butt6Click(self):
        if self.turn == "X":
            if self.Butt[6].text() != "X" and self.Butt[6].text() != "O" and self.gameBlocked != True:
                self.Butt[6].setText("X")
                self.setTurn("O")
                self.enemyBot()
    
    def Butt7Click(self):
        if self.turn == "X":
            if self.Butt[7].text() != "X" and self.Butt[7].text() != "O" and self.gameBlocked != True:
                self.Butt[7].setText("X")
                self.setTurn("O")
                self.enemyBot()
    
    def Butt8Click(self):
        if self.turn == "X":
            if self.Butt[8].text() != "X" and self.Butt[8].text() != "O" and self.gameBlocked != True:
                self.Butt[8].setText("X")
                self.setTurn("O")
                self.enemyBot()
    
    def Butt9Click(self):
        if self.turn == "X":
            if self.Butt[9].text() != "X" and self.Butt[9].text() != "O" and self.gameBlocked != True:
                self.Butt[9].setText("X")
                self.setTurn("O")
                self.enemyBot()

    def setTurn(self, xo):
        self.turn = xo
        self.turnText.setText(f"Turn: {xo}")
        self.v = self.victoryCheck()
        if self.v == "X":
            self.winText.setText("X has won!")
            self.gameBlocked = True
        if self.v == "O":
            self.winText.setText("O has won!")
            self.gameBlocked = True

    def enemyBot(self):
        if self.gameBlocked:
            print("Game ended, so bot is disabled")
        else:
            rnd = random.randint(1,9)
            if self.Butt[rnd].text() != "X" and self.Butt[rnd].text() != "O":
                self.Butt[rnd].setText("O")
                self.setTurn("X")
            else:
                self.enemyBot() # if randint fails, rerun the bot

    def victoryCheck(self):
        # X
        if self.Butt[1].text() == "X" and self.Butt[2].text() == "X" and self.Butt[3].text() == "X":
            return("X")
        if self.Butt[1].text() == "X" and self.Butt[4].text() == "X" and self.Butt[7].text() == "X":
            return("X")
        if self.Butt[1].text() == "X" and self.Butt[5].text() == "X" and self.Butt[9].text() == "X":
            return("X")
        if self.Butt[2].text() == "X" and self.Butt[5].text() == "X" and self.Butt[8].text() == "X":
            return("X")
        if self.Butt[3].text() == "X" and self.Butt[5].text() == "X" and self.Butt[7].text() == "X":
            return("X")
        if self.Butt[4].text() == "X" and self.Butt[5].text() == "X" and self.Butt[6].text() == "X":
            return("X")
        if self.Butt[3].text() == "X" and self.Butt[6].text() == "X" and self.Butt[9].text() == "X":
            return("X")
        if self.Butt[7].text() == "X" and self.Butt[8].text() == "X" and self.Butt[9].text() == "X":
            return("X")
        # O
        if self.Butt[1].text() == "O" and self.Butt[2].text() == "O" and self.Butt[3].text() == "O":
            return("O")
        if self.Butt[1].text() == "O" and self.Butt[4].text() == "O" and self.Butt[7].text() == "O":
            return("O")
        if self.Butt[1].text() == "O" and self.Butt[5].text() == "O" and self.Butt[9].text() == "O":
            return("O")
        if self.Butt[2].text() == "O" and self.Butt[5].text() == "O" and self.Butt[8].text() == "O":
            return("O")
        if self.Butt[3].text() == "O" and self.Butt[5].text() == "O" and self.Butt[7].text() == "O":
            return("O")
        if self.Butt[4].text() == "O" and self.Butt[5].text() == "O" and self.Butt[6].text() == "O":
            return("O")
        if self.Butt[3].text() == "O" and self.Butt[6].text() == "O" and self.Butt[9].text() == "O":
            return("O")
        if self.Butt[7].text() == "O" and self.Butt[8].text() == "O" and self.Butt[9].text() == "O":
            return("O")

    def resetGame(self):
        for i in range(9):
            j = i + 1
            self.Butt[j].setText("")
        self.setTurn("X")
        self.gameBlocked = False
        self.winText.setText("Nobody has won!")

if __name__ == '__main__':
    app = QApplication(sys.argv)
    ex = App()
    sys.exit(app.exec_())
