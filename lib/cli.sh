#!/bin/sh
# CLI for PLauncher
# A Launcher that sucks less
HERE=$(readlink -f $(dirname $0))

# user choices
JAVA=${JAVA:-java}
NICK=${NICK:-Player}
MEM=${MEM:-256M}
OS=${OS:-linux}
CHANNEL=${CHANNEL:-BETA} # PRECLASSIC/CLASSIC/INDEV/INFDEV/ALPHA/BETA/RELEASE
VERSION=${VERSION:-1.0.2}

getVersionJar()
{
	C=$2
	V=$3
	H=$1
	echo $H/../versions/$C/$V.jar
}

CHANNEL=$(echo $CHANNEL | tr [a-z] [A-Z])

# check if the jars are installed

if [ ! -f "$HERE/../jars/lwjgl-2.9.0.jar" ]
then
	echo "Downloading jars..."
	cd $HERE/../jars
	wget --no-check-certificate --input-file=filelist.txt
fi

# check if natives are installed

if [ ! -f "$HERE/../natives/$OS/liblwjgl.so" ]
then
	echo "Unpacking natives..."
	for b in $(ls $HERE/../jars/*platform*${OS}*.jar)
	do
		$(cat $HERE/../var/tar) xvf $b -C $HERE/../natives/$OS/
	done
	rm -rvf $HERE/../natives/$OS/META-INF
fi

# check if the version is installed

if [ ! -f "$(getVersionJar $HERE $CHANNEL $VERSION)" ]
then
	if [ "$(sh $HERE/xml.sh B${VERSION}B $HERE/../versions/$CHANNEL/manifest.xml)" == "" ]
	then
		echo "version jar missing"
		exit 1
	fi
	echo "Downloading version jar..."
	wget --no-check-certificate -O $HERE/../versions/$CHANNEL/$VERSION.jar $(sh $HERE/xml.sh B${VERSION}B $HERE/../versions/$CHANNEL/manifest.xml)
fi

echo "Version: $VERSION"
echo "Jar: $(getVersionJar $HERE $CHANNEL $VERSION)"

if [ "$VERSION" == "1.7*" ]
then
	echo "1.7 is not yet supported."
	echo "Play TicTacToe meantime"
	python3 $HERE/tictactoe.py
fi

# launch the game

JAVAP=$JAVA sh $HERE/runjar.sh $(getVersionJar $HERE $CHANNEL $VERSION) $NICK $MEM $OS $VERSION
