#!/bin/sh
# API to download mods from mcarchive.net

searchDisplay() # searchDisplay forge
{
    curl -L "https://mcarchive.net/mods?gvsn=&author=&kw=$1" | grep "/mods/" | sed 's/<a href="//g' | sed 's/?gvsn=">/ /g' | sed 's|</a>||g' | sed 's|/mods/||g'
}
downloadMod() # downloadMod minecraftforge 1.0.7
{
    export DLINK=$(curl -L https://mcarchive.net/mods/$1 | grep Download | sed 's/<a class="button" href="//g' | sed 's/">Archive Download//g' | sed 's|</a>||g' | grep "$2")
    for a in $DLINK
    do
        curl -L -O $a
    done
}
