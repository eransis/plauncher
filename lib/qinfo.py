#!/usr/bin/env python3
import sys, os, subprocess, random
from PyQt5.QtCore import *
from PyQt5.QtGui import *
from PyQt5.QtWidgets import *

args = str(sys.argv[1])

class App(QMainWindow):
    def __init__(self):
        super().__init__()
        self.w = QWidget()
        self.w.setGeometry(100,100,260,50)
        self.w.setWindowTitle("Information")
        self.text = QLabel(self.w)
        self.text.move(5, 5)
        self.text.setText(args)
        self.w.show()

if __name__ == '__main__':
    app = QApplication(sys.argv)
    ex = App()
    sys.exit(app.exec_())
