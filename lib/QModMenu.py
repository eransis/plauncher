#!/usr/bin/env python3
import os, sys, random, os.path, shutil
from PyQt5.QtCore import *
from PyQt5.QtGui import *
from PyQt5.QtWidgets import *
sys.path.append('../branding')
sys.path.append('../lang')
import Application as branding
import defaultLanguage as lang

App_Name = branding.App_Name
App_Name_Small = branding.App_Name_Small
App_Version = branding.Version

class App(QMainWindow):
    def __init__(self):
        super().__init__()

        # get the currently used channel and version
        self.chann = sys.argv[1]
        self.chann = self.chann.upper()
        self.cVer = sys.argv[2]
        
        # create the mods directories
        if not os.path.exists(f"../versions/{self.chann}/{self.cVer}-jarmods"):
            os.mkdir(f"../versions/{self.chann}/{self.cVer}-jarmods")
        if not os.path.exists(f"../versions/{self.chann}/{self.cVer}-frgmods"):
            os.mkdir(f"../versions/{self.chann}/{self.cVer}-frgmods")

        # set a live-copied version name
        self.lcVer = f"{self.cVer}_modded_2022"
        
        # the window
        self.w = QWidget()
        self.w.setWindowTitle(f"{App_Name} v{App_Version} - {lang.mods} ({self.cVer})")
        self.w.setGeometry(100,100,370,450)
        
        # Add Jar Mod
        self.addJarMod = QPushButton(self.w)
        self.addJarMod.setText(lang.addJarMod)
        self.addJarMod.resize(90,20)
        self.addJarMod.move(0,0)
        self.addJarMod.clicked.connect(self.addJarMod_act)
        
        # Add Forge (or ModLoader) Mod for supported versions
        self.addForMod = QPushButton(self.w)
        self.addForMod.setText(lang.addForgeMod)
        self.addForMod.resize(100,20)
        self.addForMod.move(90,0)
        self.addForMod.clicked.connect(self.addForMod_act)
        
        # Remove (any) mod
        self.remMod = QPushButton(self.w)
        self.remMod.setText(lang.removeMod)
        self.remMod.resize(100,20)
        self.remMod.move(270,0)
        self.remMod.clicked.connect(self.removeCheckedMods)
        
        # Run game button
        self.runGame = QPushButton(self.w)
        self.runGame.setText(lang.run)
        self.runGame.resize(80,20)
        self.runGame.move(190,0)
        self.runGame.clicked.connect(self.runPatch)
        
        self.jarMods = [ "", "", "", "", "", "", "", "", "", "", "", "", "", "", "" ]
        self.forgeMods = [ "", "", "", "", "", "", "", "", "", "", "", "", "", "", "" ]
        
        self.refreshModButtons()
        
        self.w.show()

    def runPatch(self):
        if os.path.exists(f"../versions/{self.chann}/{self.cVer}.jar"):
            # copy the jar
            shutil.copy(f"../versions/{self.chann}/{self.cVer}.jar", f"/tmp/{self.lcVer}.jar")
            
            # add mods to jar
            for n in os.listdir(f"../versions/{self.chann}/{self.cVer}-jarmods/"):
                os.system(f"sh add_mod.sh /tmp/{self.lcVer}.jar $(readlink -f ../versions/{self.chann}/{self.cVer}-jarmods/{n})")
            rString = (f"env VJAR=/tmp/{self.lcVer}.jar VERSION={self.cVer} sh cli_p.sh")
            print(f"{lang.cmd_to_be_exec}: {rString}")
            os.system(rString)
            
            # delete the modded jar
            os.remove(f"/tmp/{self.lcVer}.jar")
        else:
            print("Run the vanilla game first!")

    def refreshModButtons(self):
        im = 0
        ym = 30
        # buttons for the loaded jar mods
        for m in os.listdir(f"../versions/{self.chann}/{self.cVer}-jarmods/"):
            self.jarMods[im] = QCheckBox(self.w)
            self.jarMods[im].setText(m)
            self.jarMods[im].resize(300,20)
            self.jarMods[im].move(5,ym)
            ym = ym + 20
            im = im + 1
        self.mButtons = im - 1
        
    def addJarMod_act(self):
        try:
            self.getModZip = QFileDialog.getOpenFileName(self.w, lang.openFile, '.', f"{lang.modFiles} (*.zip *.jar)")
        except:
            print("Unable to open file")
        
        # os.system(f"sh lib/add_mod.sh {self.channel.text()} {self.ver.text()} {self.getModZip[0]}")
        shutil.copy(self.getModZip[0], f"../versions/{self.chann}/{self.cVer}-jarmods/")
        self.refreshModButtons()

    def addForMod_act(self):
        try:
            self.getModJar = QFileDialog.getOpenFileName(self.w, lang.openFile, '.', f"{lang.modFiles} (*.zip *.jar)")
        except:
            print("Unable to open file")
        
        shutil.copy(self.getModJar[0], f"../versions/{self.chann}/{self.cVer}-frgmods/")

    def removeCheckedMods(self):
        for i in range(self.mButtons + 1):
            print(i)
            if self.jarMods[i].isChecked() == True:
                print(f"DELETE: {self.jarMods[i].text()}")
                os.remove(f"../versions/{self.chann}/{self.cVer}-jarmods/{self.jarMods[i].text()}")

if __name__ == '__main__':
    app = QApplication(sys.argv)
    ex = App()
    sys.exit(app.exec_())
