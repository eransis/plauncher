#!/bin/sh
# usage: $0 minecraft.jar mod.zip
CWD=$(pwd)
TMP=/tmp/instmod012
HERE=$(readlink -f $(dirname $0))

export CHANNEL=$(echo $1 | tr [a-z] [A-Z])

export JARF=$1

rm -rf $TMP
mkdir -p $TMP

if [ ! -f "$JARF" ]
then
    python3 $HERE/qinfo.py Version_not_installed
    exit 1
fi
if [ ! -f "$2" ]
then
    python3 $HERE/qinfo.py Mod_file_not_found
    exit 1
fi

# extract the jar
cd $TMP
echo A | unzip $JARF

# extract the mod
echo A | unzip $2

# remove META-INF
rm -rf $TMP/META-INF

# repack the jar
rm -f $JARF
zip -r $JARF .
