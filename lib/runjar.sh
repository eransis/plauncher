#!/bin/sh
# run jar
# usage: env JAVAP=/path/to/java $0 /path/to/file.jar nick somethingM linux
JAVAP=${JAVAP:-java}
HERE=$(readlink -f $(dirname $0))
JARDIR=$HERE/../jars
VERJAR=$1
NICK=$2
MEM=$3
OS=$4
VVER=$5

# generate the classpath
for i in $(ls $HERE/../jars/*.jar)
do
	export CLASSPATH="$CLASSPATH$(readlink -f $i):"
done
echo "Classpath: $CLASSPATH"

export MAIN_CLASS=net.minecraft.client.Minecraft

if [ "$(cat $HERE/../var/mainclass)" == "net.minecraft.client.Minecraft" ]
then
	# vanilla
	if [ "$VVER" == "1.6" ] || [ "$VVER" == "1.6.1" ] || [ "$VVER" == "1.6.2" ] || [ "$VVER" == "1.6.3" ] || [ "$VVER" == "1.6.4" ]
	then
		export MAIN_CLASS=net.minecraft.client.main.Main
	fi
	# forge
	if [ "$VVER" == "1.6.1-forge" ] "" [ "$VVER" == "1.6.2-forge" ] || [ "$VVER" == "1.6.3-forge" ] || [ "$VVER" == "1.6.4-forge" ]
	then
		export MAIN_CLASS="net.minecraft.launchwrapper.Launch --tweakClass cpw.mods.fml.common.launcher.FMLTweaker"
	fi
fi

case $(cat $HERE/../var/enableproxy) in
	True)
		export PROXYC="-Dhttp.proxyHost=$(cat $HERE/../var/proxy)"
		;;
	*)
		export PROXYC=""
esac

$JAVAP -Xmx$MEM -XX:+UseConcMarkSweepGC -XX:+CMSIncrementalMode -XX:-UseAdaptiveSizePolicy -Xmn$MEM -Djava.library.path=$HERE/../natives/$OS -cp $CLASSPATH$VERJAR $PROXYC -Djava.util.Arrays.useLegacyMergeSort=true $MAIN_CLASS $NICK password --username $NICK --version $VVER --accessToken 12734 --gameDir $(cat $HERE/../var/game_dir)
