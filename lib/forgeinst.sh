#!/bin/sh
# Forge installer for PLauncher
# usage: forgeinst.sh channel release

CHANNEL=$1
RELEASE=$2
HERE=$(readlink -f $(dirname $0))
TMP=/tmp/forgeinst${CHANNEL}${RELEASE}
FORGEDIR=$HERE/../forge

rm -rf $TMP
mkdir -p $TMP

CHANNEL=$(echo $CHANNEL | tr [a-z] [A-z])

# return error if version is missing
if [ ! -f "$HERE/../versions/$CHANNEL/$RELEASE.jar" ]
then
    $HERE/qinfo.py "Version_does_not_exist.Download_it_first!"
    exit 1
fi

# extract the jar to temportary location
cd $TMP
unzip $HERE/../versions/$CHANNEL/$RELEASE.jar
rm -rf META-INF

# download forge
wget --no-check-certificate -O $FORGEDIR/${CHANNEL}-${RELEASE}-forge.zip $(sh $HERE/xml.sh B${RELEASE}B $FORGEDIR/${CHANNEL}.xml)

# extract forge
echo A | unzip $FORGEDIR/${CHANNEL}-${RELEASE}-forge.zip

# repack the jar
rm -rf META-INF
zip -r $HERE/../versions/${CHANNEL}/${RELEASE}-forge.jar .

# remove temportary directory
rm -rf $TMP

# show notification that it's finished
$HERE/qinfo.py "Forge_installed_to_C${CHANNEL}V${VERSION}.Its_available_as_${VERSION}-forge"
