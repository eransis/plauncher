#!/usr/bin/env python3
import sys, os, subprocess, random
try:
    from PyQt5.QtCore import *
    from PyQt5.QtGui import *
    from PyQt5.QtWidgets import *
except:
    os.system("pip install PyQt5")
    from PyQt5.QtCore import *
    from PyQt5.QtGui import *
    from PyQt5.QtWidgets import *
sys.path.append('branding')
sys.path.append('lang')
import Application as branding
import defaultLanguage as lang

# global app properties
App_Name = branding.App_Name
App_Name_Small = branding.App_Name_Small
App_Version = branding.Version


class App(QMainWindow):
    def __init__(self):
        super().__init__()
        self.w = QWidget()
        self.b = QLabel(self.w)
        self.b.setText(f"{lang.welcome} {App_Name}Qt!")
        self.w.setGeometry(100,100,370,350)
        # set the logo
        self.w.setWindowIcon(QIcon('branding/logo.png'))
        # set the logo label
        AppLogoLabel = QLabel(self.w)
        AppLogoLabel.setPixmap(QPixmap('branding/logo.png'))
        AppLogoLabel.move(1,150)
        AppLogoLabel.resize(500,200)
        # load settings
        javPatt = open("var/java", "r")
        propJava = javPatt.read()
        javPatt.close()
        nickPatt = open("var/nick", "r")
        propNick = nickPatt.read()
        nickPatt.close()
        memPatt = open("var/mem", "r")
        propMem = memPatt.read()
        memPatt.close()
        osPatt = open("var/os", "r")
        propOS = osPatt.read()
        osPatt.close()
        channPatt = open("var/channel", "r")
        propChannel = channPatt.read()
        channPatt.close()
        verPatt = open("var/ver", "r")
        propVersion = verPatt.read()
        verPatt.close()
        # properties
        self.b.move(10,10)
        self.w.setWindowTitle(f"{App_Name} v{App_Version} (Qt GUI)")
        self.jpText = QLabel(self.w)
        self.jpText.setText(f"{lang.java_path}:")
        self.jpText.move(5,30)
        self.javaPath = QLineEdit(propJava, self.w)
        self.javaPath.move(65,30)
        self.javaPath.resize(200,20)
        self.nText = QLabel(self.w)
        self.nText.setText(f"{lang.nick}:")
        self.nText.move(5,50)
        self.nick = QLineEdit(propNick, self.w)
        self.nick.move(65,50)
        self.nick.resize(100,20)
        self.mText = QLabel(self.w)
        self.mText.setText(f"{lang.memory}:")
        self.mText.move(5,70)
        self.mem = QLineEdit(propMem, self.w)
        self.mem.move(65,70)
        self.mem.resize(100,20)
        self.oText = QLabel(self.w)
        self.oText.setText(f"{lang.os}:")
        self.oText.move(5,90)
        self.osys = QLineEdit(propOS, self.w)
        self.osys.move(65,90)
        self.osys.resize(100,20)
        self.cText = QLabel(self.w)
        self.cText.setText(f"{lang.channel}:")
        self.cText.move(5,110)
        self.channel = QLineEdit(propChannel, self.w)
        self.channel.move(65,110)
        self.channel.resize(100,20)
        self.vText = QLabel(self.w)
        self.vText.setText(f"{lang.version}:")
        self.vText.move(5,130)
        self.ver = QLineEdit(propVersion, self.w)
        self.ver.move(65,130)
        self.ver.resize(100,20)
        # buttons
        self.forgeButt = QPushButton(lang.instForge, self.w)
        self.forgeButt.move(200,104)
        self.forgeButt.resize(100,20)
        self.forgeButt.clicked.connect(self.installForge)
        self.PlayButt = QPushButton(lang.play, self.w)
        self.PlayButt.move(200,124)
        self.PlayButt.resize(100,20)
        self.PlayButt.clicked.connect(self.runGame)
        self.SetButt = QPushButton(lang.saveSett, self.w)
        self.SetButt.move(200,84)
        self.SetButt.resize(100,20)
        self.SetButt.clicked.connect(self.saveSettings)
        self.WebButt = QPushButton(f"{App_Name} {lang.website}", self.w)
        self.WebButt.move(200,64)
        self.WebButt.resize(160,20)
        self.WebButt.clicked.connect(self.openWebsite)
        self.SettButt = QPushButton(lang.advanced, self.w)
        self.SettButt.clicked.connect(self.runSettings)
        self.SettButt.move(270,0)
        self.SettButt.resize(100,20)
        self.modButt = QPushButton(lang.mods, self.w)
        self.modButt.move(270,20)
        self.modButt.resize(100,20)
        self.modButt.clicked.connect(self.runModsMenu)
        self.w.show()

    def installForge(self):
        os.system(f"sh lib/forgeinst.sh {self.channel.text()} {self.ver.text()}")
        
    def runGame(self):
        # get the values
        rjava = self.javaPath.text()
        rnick = self.nick.text()
        rmem = self.mem.text()
        ros = self.osys.text()
        rchannel = self.channel.text()
        rver = self.ver.text()
        
        # print them to screen
        print(f"{lang.java_path}: ", rjava)
        print(f"{lang.nick}: ", rnick)
        print(f"{lang.memory}: ", rmem)
        print(f"{lang.os}: ", ros)
        print(f"{lang.channel}: ", rchannel)
        print(f"{lang.version}: ", rver)
        # finally, run the command with os.system
        rString = (f"env JAVA={rjava} NICK={rnick} MEM={rmem} OS={ros} CHANNEL={rchannel} VERSION={rver} sh lib/cli.sh")
        print(f"{lang.cmd_to_be_exec}: {rString}")
        os.system(rString)

    def saveSettings(self):
        print(f"{lang.setDo}...")
        os.system("rm -f var/java var/nick var/mem var/os var/channel var/ver")
        javPat = open("var/java", "w")
        javPat.write(self.javaPath.text())
        javPat.close()
        nick1 = open("var/nick", "w")
        nick1.write(self.nick.text())
        nick1.close()
        mem1 = open("var/mem", "w")
        mem1.write(self.mem.text())
        mem1.close()
        osys1 = open("var/os", "w")
        osys1.write(self.osys.text())
        osys1.close()
        chann = open("var/channel", "w")
        chann.write(self.channel.text())
        chann.close()
        verr = open("var/ver", "w")
        verr.write(self.ver.text())
        verr.close()
        print("Done")

    def openWebsite(self):
        os.system(f"/usr/bin/env xdg-open {branding.Website}")
    
    def runSettings(self):
        os.system("/usr/bin/env python3 QtSettings.py")

    def runModsMenu(self):
        os.system(f"(cd lib && python3 QModMenu.py {self.channel.text()} {self.ver.text()} )")

if __name__ == '__main__':
    app = QApplication(sys.argv)
    ex = App()
    sys.exit(app.exec_())
