#!/usr/bin/env python3
import sys, os, subprocess, random
from PyQt5.QtCore import *
from PyQt5.QtGui import *
from PyQt5.QtWidgets import *
sys.path.append('branding')
sys.path.append('lang')
import Application as branding
import defaultLanguage as lang

# global app properties
App_Name = branding.App_Name
App_Name_Small = branding.App_Name_Small
App_Version = branding.Version

class App(QMainWindow):
    def __init__(self):
        super().__init__()
        self.w = QWidget()
        self.w.setGeometry(100,100,370,350)
        self.we = QLabel(self.w)
        self.we.setText(f"{lang.apply_changes}.")
        self.we.move(5,2)
        self.w.setWindowTitle(f"{App_Name} (Qt GUI) {lang.settings}")
        self.w.setWindowIcon(QIcon('branding/logo.png'))
        AppLogoLabel = QLabel(self.w)
        AppLogoLabel.setPixmap(QPixmap('branding/logo.png'))
        AppLogoLabel.move(1,150)
        AppLogoLabel.resize(500,200)
        ### advanced settings
        self.pbX = QCheckBox(self.w)
        self.pbX.setText(f"{lang.proxy}")
        self.pbX.move(2,20)
        lpbX = open("var/enableproxy", "r")
        lstB = lpbX.read()
        if lstB == 'True':
            self.pbX.setChecked(True)
        if lstB == 'False':
            self.pbX.setChecked(False)
        lpbX.close()
        self.pbS = QLineEdit(self.w)
        lpbS = open("var/proxy", "r")
        self.pbS.setText(lpbS.read())
        self.pbS.resize(200,20)
        lpbS.close()
        self.pbS.move(170,20)
        self.saveB = QPushButton(f"{lang.apply}", self.w)
        self.saveB.clicked.connect(self.applySettings)
        self.saveB.move(280,130)
        self.saveB.resize(90,20)
        self.mcText = QLabel(self.w)
        self.mcText.setText(f"{lang.main_class}:")
        self.mcText.move(5,44)
        self.mainClass = QLineEdit(self.w)
        self.mainClass.move(170,43)
        self.mainClass.resize(200,20)
        lMc = open("var/mainclass", "r")
        self.mainClass.setText(lMc.read())
        lMc.close()
        self.tpText = QLabel(self.w)
        self.tpText.setText(f"{lang.tar_prov}:")
        self.tpText.move(5,63)
        self.tarProv = QLineEdit(self.w)
        self.tarProv.move(170,63)
        self.tarProv.resize(200,20)
        lTp = open("var/tar", "r")
        self.tarProv.setText(lTp.read())
        lTp.close()
        self.laText = QLabel(self.w)
        self.laText.setText(f"{lang.language}:")
        self.laText.move(5, 83)
        self.langSel = QLineEdit(self.w)
        self.langSel.move(170, 83)
        self.langSel.resize(200, 20)
        self.gdText = QLabel(self.w)
        self.gdText.setText(f"{lang.gamedir}:")
        self.gdText.move(5,103)
        self.gameDir = QLineEdit(self.w)
        self.gameDir.resize(200,20)
        self.gameDir.move(170,103)
        lgd = open("var/game_dir", "r")
        self.gameDir.setText(lgd.read())
        lgd.close()
        lLg = open("var/lang", "r")
        self.langSel.setText(lLg.read())
        lLg.close()
        self.w.show()

    def applySettings(self):
        print(f"{lang.setDo}...")
        os.system("rm -f var/enableproxy var/proxy var/mainclass var/tar var/lang var/game_dir")
        spbX = open("var/enableproxy", "w")
        spbX.write(str(self.pbX.isChecked()))
        spbX.close()
        spbS = open("var/proxy", "w")
        spbS.write(self.pbS.text())
        spbS.close()
        sMc = open("var/mainclass", "w")
        sMc.write(self.mainClass.text())
        sMc.close()
        sTp = open("var/tar", "w")
        sTp.write(self.tarProv.text())
        sTp.close()
        sLg = open("var/lang", "w")
        sLg.write(self.langSel.text())
        os.system(f"rm -f lang/defaultLanguage.py && cp lang/{self.langSel.text()}.py lang/defaultLanguage.py")
        sgd = open("var/game_dir", "w")
        sgd.write(self.gameDir.text())
        sgd.close()
        print(f"{lang.done}")
        sys.exit(0)


if __name__ == '__main__':
    app = QApplication(sys.argv)
    ex = App()
    sys.exit(app.exec_())
